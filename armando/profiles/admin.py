from django.contrib import admin

# profiles
from .models import Profile

class ProfileAdmin(admin.ModelAdmin):
	pass

admin.site.register(Profile, ProfileAdmin)