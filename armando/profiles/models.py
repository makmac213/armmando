from datetime import datetime
from dateutil import relativedelta
from django.db import models
from django.contrib.auth.models import User
from django_countries.fields import CountryField

# articles
from articles.models import Article

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User)
    birthday = models.DateField(null=True, blank=True)
    gender = models.CharField(max_length=15, null=True, blank=True)
    link = models.URLField(null=True, blank=True)
    hometown = models.TextField(null=True, blank=True)
    locale = models.CharField(max_length=20, null=True, blank=True)
    relationship_status = models.CharField(max_length=30, null=True, 
                                                            blank=True)
    verified = models.BooleanField(default=False)
    significant_other = models.TextField(null=True, blank=True)
    work = models.TextField(null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'profiles_profiles'

    @property
    def age(self):
        r = relativedelta.relativedelta(datetime.now(), self.birthday)
        return '%s years %s months' % (r.years, r.months)

    def __unicode__(self):
        return '%s - %s' % (self.user.get_full_name(), self.user.email)


def save_profile(backend, user, response, *args, **kwargs):
    if backend.name == 'facebook':
        try:
            profile = user.get_profile()
            if profile is None:
                profile = Profile(user_id=user.id)
        except Profile.DoesNotExist:
            profile = Profile(user_id=user.id)

        # birthday
        birthday = response.get('birthday')
        if birthday is not None:
            profile.birthday = datetime.strptime(birthday, '%m/%d/%Y')

        profile.gender = response.get('gender')
        profile.link = response.get('link')
        profile.hometown = response.get('hometown')
        profile.locale = response.get('locale')
        profile.relationship_status = response.get('relationship_status')
        profile.significant_other = response.get('significant_other')
        profile.verified = response.get('verified')
        profile.work = response.get('work')
        profile.save()


class ForgotPasswordLog(models.Model):

    user = models.ForeignKey(User, related_name='forgot_password_logs')
    code = models.CharField(max_length=64)
    is_used = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'forgot_password_logs'

    def __unicode__(self):
        return self.code
