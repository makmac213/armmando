from django.core.mail import mail_admins, EmailMultiAlternatives
from django.db import models
from django.template import Context, Template
from django.utils.html import strip_tags

# common
from common.tasks import send_mail_admin, send_mail


class SystemEmail(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug_name = models.CharField(max_length=100, db_index=True)
    subject = models.CharField(max_length=250)
    template = models.TextField()

    class Meta:
        db_table = 'common_system_emails'


    def __unicode__(self):
        return self.name

    def send(self, recipient_list, sender, context, **kwargs):
        t = Template('<html><body>%s</body></html>' % self.template)
        c = Context(context)

        html_content = t.render(c)

        attachment = kwargs.get('attachment')

        send_mail.delay(self.subject, html_content, 
                            sender, recipient_list,
                            attachment=attachment)

        print 'sending email'

    def send_admin(self, context):
        t = Template('<html><body>%s</body></html>' % self.template)
        c = Context(context)

        html_content = t.render(c)
        text_content = strip_tags(html_content)

        #mail_admins(self.subject, text_content, html_message=html_content)
        send_mail_admin.delay(self.subject, text_content, html_content)

        print 'sending email to admins'

        return html_content


class Setting(models.Model):
    key = models.CharField(max_length=100, unique=True)
    value = models.CharField(max_length=255, null=True, blank=True)
    
    class Meta:
        db_table = 'common_settings'