# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SystemEmail'
        db.create_table('common_system_emails', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('slug_name', self.gf('django.db.models.fields.CharField')(max_length=100, db_index=True)),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('template', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'common', ['SystemEmail'])

        # Adding model 'Setting'
        db.create_table('common_settings', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'common', ['Setting'])


    def backwards(self, orm):
        # Deleting model 'SystemEmail'
        db.delete_table('common_system_emails')

        # Deleting model 'Setting'
        db.delete_table('common_settings')


    models = {
        u'common.setting': {
            'Meta': {'object_name': 'Setting', 'db_table': "'common_settings'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'common.systememail': {
            'Meta': {'object_name': 'SystemEmail', 'db_table': "'common_system_emails'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'template': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['common']