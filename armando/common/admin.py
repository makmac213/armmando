from django.contrib import admin

# profiles
from .models import (SystemEmail)


class SystemEmailAdmin(admin.ModelAdmin):
    pass


admin.site.register(SystemEmail, SystemEmailAdmin)