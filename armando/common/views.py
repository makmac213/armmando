import json, time, os, string
from datetime import datetime, timedelta, date
from copy import deepcopy
from decimal import Decimal
from time import mktime

# django
from django import forms
from django.conf import settings
from django.contrib import auth, messages
from django.contrib.auth import authenticate, login, logout, get_backends
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.sessions.backends.db import Session
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db.models.loading import get_model
from django.http import HttpResponseRedirect, QueryDict, HttpResponseForbidden
from django.shortcuts import (HttpResponse, redirect, render_to_response, 
                                get_object_or_404, render)
from django.template import RequestContext, Context, Template
from django.template.defaultfilters import floatformat
from django.template.loader import render_to_string
from django.utils import timezone, translation
from django.utils.crypto import get_random_string
from django.utils.decorators import method_decorator
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import utc
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.views.generic import (FormView, TemplateView, DetailView, 
                                    ListView, UpdateView)

class LoginRequiredMixin(object):

    def dispatch(self, request, *args, **kwargs):
        ret = None
        if request.user.is_authenticated():
            ret = super(LoginRequiredMixin,
                            self).dispatch(request, *args, **kwargs)
        else:
            messages.error(request, _('Access denied'))
            ret = redirect('index')
        return ret


class StaffLoginRequiredMixin(object):

    def dispatch(self, request, *args, **kwargs):
        ret = None
        if request.user.is_authenticated() and request.user.is_staff:
            ret = super(StaffLoginRequiredMixin,
                            self).dispatch(request, *args, **kwargs)
        else:
            messages.error(request, _('Access denied'))
            ret = redirect('backoffice_login')
        return ret


class AgencyLoginRequiredMixin(object):

    def dispatch(self, request, *args, **kwargs):
        ret = None
        group = Group.objects.get(name='Agency')
        if request.user.is_authenticated() and group in request.user.groups.all():
            ret = super(AgencyLoginRequiredMixin,
                            self).dispatch(request, *args, **kwargs)
        else:
            messages.error(request, _('Access denied'))
            ret = redirect('index')
        return ret


class MemberLoginRequiredMixin(object):

    def dispatch(self, request, *args, **kwargs):
        ret = None
        group = Group.objects.get(name='Member')
        if request.user.is_authenticated() and group in request.user.groups.all():
            ret = super(MemberLoginRequiredMixin,
                            self).dispatch(request, *args, **kwargs)
        else:
            messages.error(request, _('Access denied'))
            ret = redirect('index')
        return ret
