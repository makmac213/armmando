from django.conf import settings
from django.core.mail import mail_admins, EmailMultiAlternatives
from django.utils.html import strip_tags

# djcelery
from celery import task


@task
def send_mail_admin(subject, message, html_message):
    mail_admins(subject, html_message, html_message=html_message)


@task
def send_mail(subject, html, sender, recipients, **kwargs):
    text_content = html
    attachment = kwargs.get('attachment')
    for recipient in recipients:
        try:
            # import here to avoid circular import
            from common.models import Setting
            email_setting = Setting.objects.get(key='company_email')
            bcc = email_setting.value
        except Setting.DoesNotExist:
            bcc = settings.EMAIL_NO_REPLY

        msg = EmailMultiAlternatives(subject, text_content, sender,
                                        [recipient], bcc=[bcc])
        msg.attach_alternative(html, 'text/html')

        if attachment is not None:
        	msg.attach_file(attachment)
        	
        msg.send()
