from django import forms

# Article
from .models import Article, ArticleInstructor, ArticleSupplier


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'slug', 'content', 
                    'author', 'tags',]

    def __init__(self, *args, **kwargs):
        super(ArticleForm, self).__init__(*args, **kwargs)
        self.fields['author'].widget = forms.HiddenInput()

    def save(self):
        instance = super(ArticleForm, self).save(commit=False)
        instance.save()
        return instance


class ArticleInstructorForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ArticleInstructorForm, self).__init__(*args, **kwargs)
        self.fields['article'].widget = forms.HiddenInput()        
        self.fields['user'].widget = forms.HiddenInput()        

    class Meta:
        model = ArticleInstructor


class ArticleSupplierForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ArticleSupplierForm, self).__init__(*args, **kwargs)
        self.fields['article'].widget = forms.HiddenInput()        
        self.fields['user'].widget = forms.HiddenInput()     
        self.fields['supplies'].widget = forms.Textarea()

    class Meta:
        model = ArticleSupplier
