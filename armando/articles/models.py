from django.db import models
from django.contrib.auth.models import User


class CommonModelManager(models.Manager):
    def get_queryset(self):
        return models.query.QuerySet(self.model).filter(is_deleted=False)


class Category(models.Model):
    name = models.CharField(max_length=255)
    parent = models.ForeignKey('self', null=True, blank=True)

    class Meta:
        db_table = 'articles_categories'
        verbose_name_plural= 'categories'

    def __unicode__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'articles_tags'

    def __unicode__(self):
        return self.name


class Article(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(null=True, blank=True, unique=True)
    content = models.TextField()
    author = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    visits = models.IntegerField(default=0)
    # related fields
    categories = models.ManyToManyField(Category, null=True, blank=True)
    tags = models.ManyToManyField(Tag, null=True, blank=True)

    class Meta:
        db_table = 'articles_articles'

    def __unicode__(self):
        return self.title

        
class Comment(models.Model):
    author = models.ForeignKey(User, related_name='comments')
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'articles_comments'


class ArticleInstructor(models.Model):
    article = models.ForeignKey(Article, related_name='instructors')
    user = models.ForeignKey(User, related_name='my_skills')
    contact = models.CharField(max_length=11)

    class Meta:
        db_table = 'articles_instructors'
        unique_together = (
                ('article', 'user'),
            )

    def __unicode__(self):
        return '%s - %s' % (self.article.title, self.article.user.get_full_name())


class ArticleSupplier(models.Model):
    article = models.ForeignKey(Article, related_name='suppliers')
    user = models.ForeignKey(User, related_name='my_supplies')
    contact = models.CharField(max_length=11)
    supplies = models.CharField(max_length=100)

    class Meta:
        db_table = 'articles_supplier'
        unique_together = (
                ('article', 'user'),
            )

    def __unicode__(self):
        return '%s - %s' % (self.article.title, self.article.user.get_full_name())
