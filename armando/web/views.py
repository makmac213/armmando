import json, time, os, string
from datetime import datetime, timedelta, date
from copy import deepcopy
from decimal import Decimal
from time import mktime

# django
from django import forms
from django.conf import settings
from django.contrib import auth, messages
from django.contrib.auth import authenticate, login, logout, get_backends
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.sessions.backends.db import Session
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db.models.loading import get_model
from django.http import HttpResponseRedirect, QueryDict, HttpResponseForbidden
from django.shortcuts import (HttpResponse, redirect, render_to_response, 
                                get_object_or_404, render)
from django.template import RequestContext, Context, Template
from django.template.defaultfilters import floatformat
from django.template.loader import render_to_string
from django.utils import timezone, translation
from django.utils.crypto import get_random_string
from django.utils.decorators import method_decorator
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import utc
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.views.generic import (FormView, TemplateView, DetailView, 
                                    ListView, UpdateView)

# articles
from articles.forms import ArticleInstructorForm, ArticleSupplierForm
from articles.models import (Article, ArticleInstructor, ArticleSupplier, 
                                Category)

# common
from common.models import SystemEmail
from common.views import LoginRequiredMixin, MemberLoginRequiredMixin

# profiles
from profiles.models import Profile

# web
from .forms import LoginForm, SignupForm, ForgotPasswordForm, ResetPasswordForm


class FrontendView(object):

    class Index(TemplateView):
        template_name = 'web/index.html'

        def get_context_data(self, **kwargs):
            context = super(FrontendView.Index,
                                self).get_context_data(**kwargs)
            object_list = Article.objects.filter().order_by('-modified')

            paginator = Paginator(object_list, settings.DEFAULT_POST_COUNT)
            page = self.request.GET.get('page')
            try:
                object_list = paginator.page(page)
            except PageNotAnInteger:
                object_list = paginator.page(1)
            except EmptyPage:
                object_list = paginator.page(paginator.num_pages)

            context['object_list'] = object_list
            return context

    class AboutUs(TemplateView):
        template_name = 'web/about_us.html'

    class Signup(View):

        def post(self, request, *args, **kwargs):
            form = SignupForm(request.POST)

            if form.is_valid():
                member_group = Group.objects.get(id=settings.GROUP_MEMBER)
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')
                # create user
                user = User()
                user.username = username
                user.email = username
                user.set_password(password)
                user.save()
                # add to group
                group = Group.objects.get(id=settings.GROUP_MEMBER)
                user.groups.add(group)
                user.save()
                # link profile
                profile = Profile()
                profile.user = user
                profile.save()
                # link job seeker profile                
                #job_seeker_profile = JobSeekerProfile()
                #job_seeker_profile.user = user
                #job_seeker_profile.save()
                # login current user
                auth_user = authenticate(username=username, password=password)
                if auth_user is not None:
                    if auth_user.is_active:
                        login(request, auth_user)
                    else:
                        # use same error message to avoid usename-sniffing bots
                        messages.error(request, _('Invalid username and/or password'))    
                # send welcome email to user
                #welcome_email = SystemEmail.objects.get(slug_name='welcome-email')
                #subject = welcome_email.subject
                #content = unicode(_('You can reset your at'))
                #base_url = '%s' % (settings.BASE_URL)
                #content += ' %s' % (base_url)
                #send_mail.delay(subject, content,
                #                    settings.EMAIL_NO_REPLY, [username])
                #subject = welcome_email.subject
                #sender = settings.EMAIL_NO_REPLY
                #email_context = {
                #    'username': user.email,
                #    'customer_id': user.id,                    
                #}
                #welcome_email.send([username], settings.EMAIL_NO_REPLY,
                #                        email_context)
                messages.success(request, _('You are now logged in.'))
                return redirect('index')
            else:
                print form.errors
                return redirect('index')
            context = {
                'form': form,
            }
            return render(request, self.template_name, context)


    class Logout(View):
        def get(self, request, *args, **kwargs):
            logout(request)
            return redirect(reverse('index')) 


    class Login(View):

        def post(self, request, *args, **kwargs):
            # logout user if currently logged in
            logout(request)
            # redirect user on successful login
            next = request.GET.get('next', 'index')

            form = LoginForm(request.POST)
            if form.is_valid():
                # form is valid try to authenticate username and password
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')
                user = authenticate(username=username, password=password)
                # user found and authenticated
                if user is not None:
                    if user.is_active:
                        login(request, user)
                    else:
                        # use same error message to avoid usename-sniffing bots
                        messages.error(request, _('Invalid username and/or password'))    
                else:
                    messages.error(request, _('Invalid username and/or password'))
            else:
                print form.errors
            return redirect(next)


class BlogView(object):

    class List(ListView):
        template_name = 'web/blogs/list.html'
        model = Article
        queryset = Article.objects.filter(categories__name='Blogs').order_by('-modified')
        paginate_by = settings.DEFAULT_POST_COUNT


    class Details(DetailView):
        template_name = 'web/blogs/details.html'
        model = Article

        def get_context_data(self, **kwargs):
            context = super(BlogView.Details, self).get_context_data(**kwargs)
            obj = context.get('object')
            try:
                is_favorite = False
                profile = self.request.user.job_seeker_profile
                if profile.favorite_articles.filter(id=obj.id).count():
                    is_favorite = True
                context['is_favorite'] = is_favorite
            except Exception, e:
                print e
            return context

    class AddFavorite(LoginRequiredMixin, View):
        def post(self, request, *args, **kwargs):
            pk = int(kwargs.get('pk', 0))
            article = get_object_or_404(Article, id=pk)
            profile = request.user.job_seeker_profile
            profile.favorite_articles.add(article)
            messages = ['Blog added to favorites']
            context = {
                'error': False,
                'messages': messages
            }
            return HttpResponse(json.dumps(context))

        @method_decorator(csrf_exempt)
        def dispatch(self, *args, **kwargs):
            return super(BlogView.AddFavorite, self).dispatch(*args, **kwargs)


    class DeleteFavorite(LoginRequiredMixin, View):
        def post(self, request, *args, **kwargs):
            pk = int(kwargs.get('pk', 0))
            article = get_object_or_404(Article, id=pk)
            profile = request.user.job_seeker_profile
            profile.favorite_articles.remove(article)
            messages = ['Blog removed from favorites']
            context = {
                'error': False,
                'messages': messages
            }
            return HttpResponse(json.dumps(context))

        @method_decorator(csrf_exempt)
        def dispatch(self, *args, **kwargs):
            return super(BlogView.DeleteFavorite, self).dispatch(*args, **kwargs)


class GuidesView(object):

    class List(ListView):
        template_name = 'web/guides/list.html'
        model = Article
        queryset = Article.objects.filter(categories__name='Guides')
        paginate_by = settings.DEFAULT_POST_COUNT


    class Details(DetailView):
        template_name = 'web/guides/details.html'
        model = Article

        def get_context_data(self, **kwargs):
            context = super(GuidesView.Details, self).get_context_data(**kwargs)
            obj = context.get('object')
            try:
                is_favorite = False
                profile = self.request.user.job_seeker_profile
                if profile.favorite_articles.filter(id=obj.id).count():
                    is_favorite = True
                context['is_favorite'] = is_favorite
            except Exception, e:
                print e
            return context

    class AddFavorite(LoginRequiredMixin, View):
        def post(self, request, *args, **kwargs):
            pk = int(kwargs.get('pk', 0))
            article = get_object_or_404(Article, id=pk)
            profile = request.user.job_seeker_profile
            profile.favorite_articles.add(article)
            messages = ['Article added to favorites']
            context = {
                'error': False,
                'messages': messages
            }
            return HttpResponse(json.dumps(context))

        @method_decorator(csrf_exempt)
        def dispatch(self, *args, **kwargs):
            return super(GuidesView.AddFavorite, self).dispatch(*args, **kwargs)


    class DeleteFavorite(LoginRequiredMixin, View):
        def post(self, request, *args, **kwargs):
            pk = int(kwargs.get('pk', 0))
            article = get_object_or_404(Article, id=pk)
            profile = request.user.job_seeker_profile
            profile.favorite_articles.remove(article)
            messages = ['Article removed from favorites']
            context = {
                'error': False,
                'messages': messages
            }
            return HttpResponse(json.dumps(context))

        @method_decorator(csrf_exempt)
        def dispatch(self, *args, **kwargs):
            return super(GuidesView.DeleteFavorite, 
                            self).dispatch(*args, **kwargs)

    class NewInstructor(LoginRequiredMixin, View):
        template_name = 'web/guides/new_instructor.html'

        def get(self, request, *args, **kwargs):
            pk = kwargs.get('pk')
            article = get_object_or_404(Article, id=int(pk))
            if ArticleInstructor.objects.filter(id=int(pk), user=request.user).count():
                return redirect(reverse('guides_details', args=[pk]))
            initial = {
                'article': article.id,
                'user': request.user.id,
            }

            form = ArticleInstructorForm(initial=initial)
            context = {
                'form': form,
                'article': article,
            }
            return render(request, self.template_name, context)

        def post(self, request, *args, **kwargs):
            pk = kwargs.get('pk')
            article = get_object_or_404(Article, id=int(pk))

            form = ArticleInstructorForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect(reverse('guides_details', args=[pk]))
            else:
                print form.errors

            context = {
                'form': form,
                'article': article,
            }
            return render(request, self.template_name, context)

    class EditInstructor(LoginRequiredMixin, View):
        template_name = 'web/guides/new_instructor.html'

        def get(self, request, *args, **kwargs):
            pk = kwargs.get('pk')
            article = get_object_or_404(Article, id=int(pk))
            instance = get_object_or_404(ArticleInstructor, article=article,
                                            user=request.user)
            form = ArticleInstructorForm(instance=instance)
            context = {
                'form': form,
                'article': article,
                'instance': instance
            }
            return render(request, self.template_name, context)

        def post(self, request, *args, **kwargs):
            pk = kwargs.get('pk')
            article = get_object_or_404(Article, id=int(pk))
            instance = get_object_or_404(ArticleInstructor, article=article,
                                            user=request.user)
            form = ArticleInstructorForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect(reverse('guides_details', args=[pk]))
            else:
                print form.errors

            context = {
                'form': form,
                'article': article,
                'instance': instance,
            }
            return render(request, self.template_name, context)

    class NewSupplier(LoginRequiredMixin, View):
        template_name = 'web/guides/new_supplier.html'

        def get(self, request, *args, **kwargs):
            pk = kwargs.get('pk')
            article = get_object_or_404(Article, id=int(pk))
            if ArticleSupplier.objects.filter(id=int(pk), user=request.user).count():
                return redirect(reverse('guides_details', args=[pk]))
            initial = {
                'article': article.id,
                'user': request.user.id,
            }

            form = ArticleSupplierForm(initial=initial)
            context = {
                'form': form,
                'article': article,
            }
            return render(request, self.template_name, context)

        def post(self, request, *args, **kwargs):
            pk = kwargs.get('pk')
            article = get_object_or_404(Article, id=int(pk))

            form = ArticleSupplierForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect(reverse('guides_details', args=[pk]))
            else:
                print form.errors

            context = {
                'form': form,
                'article': article,
            }
            return render(request, self.template_name, context)

    class EditSupplier(LoginRequiredMixin, View):
        template_name = 'web/guides/new_supplier.html'

        def get(self, request, *args, **kwargs):
            pk = kwargs.get('pk')
            article = get_object_or_404(Article, id=int(pk))
            instance = get_object_or_404(ArticleSupplier, article=article,
                                            user=request.user)
            form = ArticleSupplierForm(instance=instance)
            context = {
                'form': form,
                'article': article,
                'instance': instance
            }
            return render(request, self.template_name, context)

        def post(self, request, *args, **kwargs):
            pk = kwargs.get('pk')
            article = get_object_or_404(Article, id=int(pk))
            instance = get_object_or_404(ArticleSupplier, article=article,
                                            user=request.user)
            form = ArticleSupplierForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect(reverse('guides_details', args=[pk]))
            else:
                print form.errors

            context = {
                'form': form,
                'article': article,
                'instance': instance,
            }
            return render(request, self.template_name, context)


class FacebookView(View):

    class NewAssociation(View):

        def get(self, request, *args, **kwargs):
            social_user = request.user.social_auth.filter(provider="facebook")[0]

            return redirect('index')

