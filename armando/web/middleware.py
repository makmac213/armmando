
class UserAgentTrackingMiddleware(object):

    def process_request(self, request):
        print request.user_agent.is_mobile # returns True
        print request.user_agent.is_tablet # returns False
        print request.user_agent.is_touch_capable # returns True
        print request.user_agent.is_pc # returns False
        print request.user_agent.is_bot # returns False

        # Accessing user agent's browser attributes
        print request.user_agent.browser  # returns Browser(family=u'Mobile Safari', version=(5, 1), version_string='5.1')
        print request.user_agent.browser.family  # returns 'Mobile Safari'
        print request.user_agent.browser.version  # returns (5, 1)
        print request.user_agent.browser.version_string   # returns '5.1'

        # Operating System properties
        print request.user_agent.os  # returns OperatingSystem(family=u'iOS', version=(5, 1), version_string='5.1')
        print request.user_agent.os.family  # returns 'iOS'
        print request.user_agent.os.version  # returns (5, 1)
        print request.user_agent.os.version_string  # returns '5.1'

        # Device properties
        print request.user_agent.device  # returns Device(family='iPhone')
        print request.user_agent.device.family  # returns 'iPhone'
        print request.get_full_path()
        print request.META.get('HTTP_REFERER')
        print request.build_absolute_uri(request.get_full_path())