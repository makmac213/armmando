from django import forms
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _


class LoginForm(forms.Form):

    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        return self.cleaned_data


class SignupForm(forms.Form):
    username = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    confrim_password = forms.CharField(widget=forms.PasswordInput)

    def clean_password(self):
        password = self.cleaned_data['password']
        if len(password) < 6:
            raise forms.ValidationError(_("Password must have at least 6 characters."))

        if len(password)>80:
            raise forms.ValidationError(_("Password must not be longer than 80 characters."))
        return password

    def clean(self):
        cleaned_data = self.cleaned_data
        username = cleaned_data.get('username')

        query = Q(username=username)
        query.add(Q(email=username), Q.OR)
        if User.objects.filter(query).count():
            self._errors['username'] = self.error_class(
                                        [_('Username/email already in use')])

        if 'password' in cleaned_data and 'confirm_password' in cleaned_data:
            password = cleaned_data['password']
            confirm_password = cleaned_data['confirm_password']

            if password != confirm_password:
                self._errors['password'] = self.error_class(["Password do not match"])

                del cleaned_data['password']
                del cleaned_data['confirm_password']
                
        return cleaned_data

class ForgotPasswordForm(forms.Form):
    email = forms.EmailField()

    
class ResetPasswordForm(forms.Form):
    password = forms.CharField(label=_("Enter Password"), 
                    widget=forms.PasswordInput(
                        attrs={'placeholder': _('Enter new password')}), 
                        error_messages={'required':_('Enter a valid password.')})
    confirm_password = forms.CharField(label=_("Confirm Password"), 
                            widget=forms.PasswordInput(
                                attrs={'placeholder': _('Re-enter password')}), 
                                error_messages={'required':_('Re-enter password.')})

    def clean_password(self):
        password = self.cleaned_data['password']

        if len(password) < 6:
            raise forms.ValidationError(_("Password must have at least 6 characters."))

        if len(password)>80:
            raise forms.ValidationError(_("Password must not be longer than 80 characters."))

        return password


    def clean(self):
        cleaned_data = self.cleaned_data

        if 'password' in cleaned_data and 'confirm_password' in cleaned_data:
            password = cleaned_data['password']
            confirm_password = cleaned_data['confirm_password']

            if password != confirm_password:
                self._errors['password'] = self.error_class(["Password do not match"])

                del cleaned_data['password']
                del cleaned_data['confirm_password']

        return cleaned_data